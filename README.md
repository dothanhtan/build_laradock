# Sử dụng Laradock để build môi trường DEV cho Laravel

## Laradock là gì
> Laradock is a full PHP development environment based on Docker.

## Tạo môi trường dev cho laravel
Trước khi bắt đầu các bước cài đặt môi trường dev cho laravel bằng laradock thì bạn phải chắc chắn là đã cài docker vs git
- Download LaraDock
```bash
# Tạo thư mục
$ mkdir laravel_docker
# Di chuyển đến thư mục vừa tạo
$ cd laravel_docker
# Clone LaraDock
$ git clone https://github.com/LaraDock/laradock.git
```

- Khởi tạo docker
```bash
# Di chuyển đến thư mục laradock
$ cd laradock
# Copy file .env
$ cp .env.example .env
# Khởi động container
$ docker-compose up -d nginx mysql workspace phpmyadmin
# Confirm lại xem các container vừa được khởi động ở bước trên đã chạy chưa.
$ docker ps
CONTAINER ID        IMAGE                 COMMAND                  CREATED             STATUS              PORTS                                      NAMES
42218ff4fdeb        laradock_phpmyadmin   "/docker-entrypoint.…"   10 seconds ago      Up 8 seconds        0.0.0.0:8080->80/tcp                       laradock_phpmyadmin_1
ce8875359faa        laradock_nginx        "/bin/bash /opt/star…"   47 seconds ago      Up 9 seconds        0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp   laradock_nginx_1
229e9c9b0fb9        laradock_php-fpm      "docker-php-entrypoi…"   49 seconds ago      Up 47 seconds       9000/tcp                                   laradock_php-fpm_1
3f398da51a14        laradock_workspace    "/sbin/my_init"          50 seconds ago      Up 48 seconds       0.0.0.0:2222->22/tcp                       laradock_workspace_1
761b70cc0bd2        laradock_mysql        "docker-entrypoint.s…"   51 seconds ago      Up 10 seconds       0.0.0.0:3306->3306/tcp, 33060/tcp          laradock_mysql_1
8f28241ced91        docker:dind           "dockerd-entrypoint.…"   51 seconds ago      Up 49 seconds       2375-2376/tcp                              laradock_docker-in-docker_1
```

- Tạo project laravel
```bash
# Kết nối vs container workspace vừa được tạo ở trên
$ docker-compose exec --user=laradock workspace bash
# Tạo project laravel
$ composer create-project laravel/laravel sampleapp --prefer-dist "8.*.*"
# Thoát ra khởi container
$ exit
```

- Sau khi đã tạo xong project thì thay đổi file .env như dưới đây:
    - Trước khi thay đổi:
    ```
    # Point to the path of your applications code on your host
    APP_CODE_PATH_HOST=../
    ```
    - Sau khi thay đổi:
    ```
    # Point to the path of your applications code on your host
    APP_CODE_PATH_HOST=../sampleapp
    ```

- Sau khi thay đổi xong thì cần khởi động lại container để các container cần thiết có thể nhận được setting mới.
```bash
# サービスの停止
$ docker-compose stop
# サービスの起動
$ docker-compose up -d nginx mysql
```

Sau khi thiết lập xong thì các bạn có thể kiểm tra lại kết quả xem đã cài đặt thành công hay chưa bằng cách truy nhập vào địa chỉ [http://localhost](http://localhost). Nếu cài đặt thành công thì sẽ xuất hiện màn hình wellcome của Laravel.

- Kiểm tra xem php artisan migrate có thể chạy được hay không?
Kiểm tra xem setting connect DB ở file .env của laravel đã setup đúng chưa?
```dotenv
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=database_name
DB_USERNAME=root
DB_PASSWORD=root
```

- Sau khi thay đổi như trên thì ta có thể chạy được php artisan migrate
```bash
laradock@fd5cdbe63d4b:/var/www$ php artisan migrate
Migration table created successfully.
Migrating: 2014_10_12_000000_create_users_table
Migrated:  2014_10_12_000000_create_users_table (0.07 seconds)
Migrating: 2014_10_12_100000_create_password_resets_table
Migrated:  2014_10_12_100000_create_password_resets_table (0.04 seconds)
Migrating: 2019_08_19_000000_create_failed_jobs_table
Migrated:  2019_08_19_000000_create_failed_jobs_table (0.02 seconds)
```
